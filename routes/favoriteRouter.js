const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const authenticate = require('../authenticate');
const cors = require('./cors');

const favoriteRouter = express.Router();

const Favorites = require('../models/favorite');

favoriteRouter.use(bodyParser.json());

favoriteRouter.route('/')
.options(cors.corsWithOptions, (req, res) => { 
    res.sendStatus(200);
})
.get(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    Favorites.find({user: req.user._id})
    .populate('dishes user')
    .then((favorites) => {
        res.statusCode = 200;
        res.json(favorites);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.post(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
   Favorites.findOne({user: req.user._id}, (err, favorite)=> {
       if (err) return next(err);
       else {
           if (!favorite) {
                favorite = new Favorites({
                    user: req.user._id
                })
           }
           for(let dish of req.body) {
                if (favorite.dishes.indexOf(dish._id) === -1) {
                    favorite.dishes.push(dish);
                }
            }
           favorite.save()
            .then((favorite) => {
                Favorites.findById(favorite._id)
                .populate('user dishes')
                .then((favorite) => {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(favorite);
                })
                
            }, (err) => next(err));
       }
   })
   .catch((err) => next(err));
})
.put(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    res.statusCode = 403;
    res.end('PUT operation not supported on /favorite');
})
.delete(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    Favorites.remove({user: req.user._id})
    .then((resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
    }, (err) => next(err))
    .catch((err) => next(err));
});

favoriteRouter.route('/:dishId')
.options(cors.corsWithOptions, (req, res) => { 
    res.sendStatus(200);
})
.get(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    Favorites.findOne({user: req.user._id})
    .then((favorites) => {
        if (!favorites) {
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            return res.json({"exists": false, "favorite": favorites });
        }
        else {
            if (favorites.dishes.indexOf(req.params.dishId) < 0) {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                return res.json({"exists": false, "favorite": favorites });
            }
            else {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                return res.json({"exists": true, "favorite": favorites });
            }
        }
    }, (err) => next(err))
    .catch((err) => next(err))
})
.post(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    Favorites.findOne({user: req.user._id}, (err, favorite)=> {
        if (err) return next(err);
        else {
            if (!favorite) {
                 favorite = new Favorites({
                     user: req.user._id
                 })
            }
            favorite.dishes.push(req.params.dishId);
            favorite.save()
             .then((favorite) => {
                Favorites.findById(favorite._id)
                .populate('user dishes')
                .then((favorite) => {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(favorite);
                })
             }, (err) => next(err));
        }
    })
    .catch((err) => next(err));
 })
 .put(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    res.statusCode = 403;
    res.end('PUT operation not supported on /favorite/:dishId');
})
 .delete(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    Favorites.findOne({user: req.user._id}, (err, favorite)=> {
        if (err) return next(err);
        else {
            if (favorite != null && favorite.dishes != null) {
                favorite.dishes.remove({_id: req.params.dishId})
                favorite.save()
                .then((favorite) => {
                    Favorites.findById(favorite._id)
                    .populate('user dishes')
                    .then((favorites) => {
                        res.statusCode = 200;
                        res.setHeader('Content-Type', 'application/json');
                        res.json(favorite);
                    })
                }, (err) => next(err));
            }
            else {
                err = new Error('Dish ' + req.params.dishId + ' not found in your favorites');
                err.status = 404;
                return next(err);
            }
            
        }
    })
    .catch((err) => next(err));
});

module.exports = favoriteRouter;